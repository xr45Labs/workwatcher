import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter/rendering.dart';
import 'package:workwatcher/Model/Motor.dart';
import 'package:workwatcher/bloc/blocNewMotor.dart';

import 'Pages/HomePage.dart';

class NewMotor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Nuevo motor'),

      ),
      body: Container(
        //margin: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            newMotorClient(),
            newMotorDescription(),
            saveMotor(context)
          ],
        ),
      ),
    );
  }
/*
  Widget newMotorClient() {
    String error;
    return TextField(
      enabled: true,
      style: TextStyle(fontSize: 24.0, color: Colors.blue),
      textAlign: TextAlign.center,
      decoration: InputDecoration(
          labelText: 'Cliente/Empresa:',
          hintText: 'Nombre  cliente/empresa',
          errorText: error

      ),
      onChanged: (text){
        if(text.length < 5){
          error = "Ingrese un dato valido";
        }
      },

    );
  }*/
/*
  Widget newMotorDescription(){
    String error;
    return TextField(
            autofocus: true,
            maxLines: 5,
            decoration: InputDecoration(
                labelText: 'Descripcion',
                hintText: 'Agregue una descripcion',
                errorText: error
            ),
          );
  }*/

  Widget saveMotor(BuildContext context){
    return RaisedButton(
      child: Text('Guardar motor'),
      onPressed: (){

        blocNewMotor.Dispose();
        //Navigator.pop(context);
        //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
      },
    );
  }


  Widget newMotorClient() {
    return StreamBuilder(
        stream: blocNewMotor.client,
        builder: (context, snapshot) {
          return TextField(
            onChanged: blocNewMotor.changeClient,
            enabled: true,
            style: TextStyle(fontSize: 24.0, color: Colors.blue),
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              labelText: 'Cliente/Empresa:',
                hintText: 'Nombre  cliente/empresa',
                errorText: snapshot.error
            ),
          );
        });
  }

  Widget newMotorDescription(){
    return StreamBuilder(
          stream: blocNewMotor.description,
          builder: (context,snapshot){
            return TextField(
              onChanged: blocNewMotor.changeDescription,
              autofocus: true,
              maxLines: 5,
              decoration: InputDecoration(
                labelText: 'Descripcion',
                  hintText: 'Agregue una descripcion',
                  errorText: snapshot.error
              ),
            );
          }
      );

  }
}
