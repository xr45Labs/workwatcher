/*class Motor{
  int id;
  String code;
  String client;
  String receiveDate;
  int status;

  Motor(this.id,this.code,this.client,this.receiveDate,this.status  );
}*/

import 'package:cloud_firestore/cloud_firestore.dart';

class Motor {
  int id;
  String code;
  String client;
  String receiveDate;
  int status;
  DocumentReference reference;

  //Motor(this.id,this.code,this.client,this.receiveDate,this.status  );

  Motor.fromMap(Map<String, dynamic> map, {this.reference})
      :
        assert(map['id'] != null),
        assert(map['code'] != null),
        assert(map['client'] != null),
        assert(map['receiveDate'] != null),
        assert(map['status'] != null),
        id = map['id'],
        code = map['code'],
        client = map['client'],
        receiveDate = map['receiveDate'],
        status = map['status'];

  Motor.fromSnapshot(DocumentSnapshot snapshot) :
        this.fromMap(snapshot.data, reference: snapshot.reference);
}