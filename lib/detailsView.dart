import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:workwatcher/Model/Motor.dart';

class DetailsView extends StatefulWidget {
  Motor motorDetails;
  DetailsView(Motor motor) {
    this.motorDetails = motor;
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DetailsViewState(this.motorDetails);
  }
}

class DetailsViewState extends State<DetailsView> {
  Motor motorDetails;
  DetailsViewState(Motor motor) {
    this.motorDetails = motor;
  }

  TextEditingController _idTFController = new TextEditingController();
  TextEditingController _clientTFController = new TextEditingController();
  TextEditingController _dateTFController = new TextEditingController();
  TextEditingController _statusTFController = new TextEditingController();
  TextEditingController _descriptionTFController = new TextEditingController();

  @override
  void initState() {
    _idTFController.text = this.motorDetails.code;
    _clientTFController.text = this.motorDetails.client;
    _dateTFController.text = this.motorDetails.receiveDate;
    if (this.motorDetails.status == 0) {
      _statusTFController.text = 'Nuevo';
    } else if (this.motorDetails.status == 1) {
      _statusTFController.text = 'Reparacion';
    } else if (this.motorDetails.status == 2) {
      _statusTFController.text = 'Terminado';
    }
    return super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Detalles'),
      ),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Text(
            'Code:',
            textAlign: TextAlign.start,
          ),
          TextField(
            enabled: false,
            controller: _idTFController,
            style: TextStyle(fontSize: 24.0, color: Colors.blue),
            textAlign: TextAlign.center,
          ),
          Text(
            'Cliente/Empresa:',
            textAlign: TextAlign.start,
          ),
          TextField(
            enabled: false,
            controller: _clientTFController,
            style: TextStyle(fontSize: 24.0, color: Colors.blue),
            textAlign: TextAlign.center,
          ),
          Text(
            'Fecha de ingreso:',
            textAlign: TextAlign.start,
          ),
          TextField(
            enabled: false,
            controller: _dateTFController,
            style: TextStyle(fontSize: 24.0, color: Colors.blue),
            textAlign: TextAlign.center,
          ),
          Text(
            'Status:',
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Icon(Icons.input,
                        size: 52,
                        color: motorDetails.status == 0 ? Colors.blue : null),
                    Text('Nuevo',
                        style: TextStyle(
                            color:
                                motorDetails.status == 0 ? Colors.blue : null))
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(Icons.build,
                        size: 52,
                        color: motorDetails.status == 1 ? Colors.blue : null),
                    Text(
                      'Reparacion',
                      style: TextStyle(
                          color: motorDetails.status == 1 ? Colors.blue : null),
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Icon(Icons.done,
                        size: 52,
                        color: motorDetails.status == 2 ? Colors.blue : null),
                    Text('Terminado',
                        style: TextStyle(
                            color:
                                motorDetails.status == 2 ? Colors.blue : null))
                  ],
                )
              ],
            ),
            padding: EdgeInsets.only(top: 24.0),
          ),
          /*TextField(
                enabled: false,
                controller: _statusTFController,
                style: TextStyle(fontSize: 24.0, color: Colors.blue),
                textAlign: TextAlign.center,
              ),*/
          motorDetails.status != 2
              ? Container(
                  padding: EdgeInsets.only(top: 25.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Descriptcion:',
                      ),
                      TextField(
                        autofocus: true,
                        maxLines: 5,
                      ),
                      RaisedButton(
                          color: Colors.blue,
                          child: Text('Siguiente fase'),
                          onPressed: () {
                            Firestore.instance.runTransaction(
                              (transaction) async {
                                await transaction.update(motorDetails.reference,
                                    {'status': motorDetails.status + 1});
                              },
                            );
                            Navigator.pop(context);
                          })
                    ],
                  ),
                )
              : Container(
                  padding: EdgeInsets.only(top: 25),
                  child: Card(
                      child: ListTile(
                          leading: Icon(Icons.done_all, color: Colors.blue),
                          title: Text('El motor esta listo para ser entregado.',
                              style: TextStyle(
                                  fontSize: 24.0, color: Colors.blue)))),
                )
        ]),
      ),
    );
  }
}
