import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:workwatcher/Model/Motor.dart';
import 'package:workwatcher/detailsView.dart';

  Widget buildBody(BuildContext context, int tabSection) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('Motor').where('status', isEqualTo: tabSection).snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: SizedBox(
                height: 50.0,
                width: 50.0,
                child: CircularProgressIndicator()),
          );
        }
        print(snapshot.data);
        return _buildList(context, snapshot.data.documents);
      },
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: snapshot.map((data) => _buildListItem(context, data)).toList()
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
    final motor = Motor.fromSnapshot(data);

    return Container(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.insert_drive_file),
              title: Text(motor.code),
              subtitle: Text(motor.client),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DetailsView(motor)),
                );
              },
            )
            /*Text(
              motor.receiveDate,
              textAlign: TextAlign.right,
              textDirection: TextDirection.rtl,
            )*/
          ],
        ),
      ),
    );

    /*
    return Padding(
      key: ValueKey(motor.code),
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: ListTile(
          title: Text(motor.code),
          trailing: Text(motor.client),
          onTap: () => Firestore.instance.runTransaction((transaction) async {
            final freshSnapshot = await transaction.get(motor.reference);
            final fresh = Motor.fromSnapshot(freshSnapshot);

            await transaction
                .update(motor.reference, {'votes': fresh.status + 1});
          }),
        ),
      ),
    );*/

    void Dispose(){

    }
  }


