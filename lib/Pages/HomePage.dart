import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:qr_reader/qr_reader.dart';
import 'package:workwatcher/FirebaseServices/MotorService.dart';

import '../NewMotor.dart';



class HomePage extends StatefulWidget {


  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  int _indexTab = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new DefaultTabController(
        length: choices.length,
        child: Scaffold(
          appBar: AppBar(
            title: new Text('WorkWatcher'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => NewMotor()));
                  //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => NewMotor()));
                },
              ),
            ],
            bottom: _TabBarWidget(),
          ),
          body: _TabBarViewWidget(context),
          floatingActionButton: _floatingButtonWidget()
        ));
  }

  Widget _TabBarWidget(){
    return TabBar(
      isScrollable: false,
      tabs: choices.map((Choice choice) {
        return Tab(
          text: choice.status,
          icon: Icon(choice.icon),
        );
      }).toList(),
      onTap: (tabIndex) {/*
                setState(() {
                  listFilter(tabIndex);
                });*/

        setState(() {
          _indexTab = tabIndex;
          //_list = buildBody(this.context, tabIndex);
        });

      },
    );
  }

  Widget _TabBarViewWidget(BuildContext context){
    return buildBody(context,_indexTab);
    /*TabBarView(
        physics: NeverScrollableScrollPhysics(),
        children: choices.map((Choice choice) {
          return buildBody(context,_indexTab);
          /*
          return Padding(
              padding: const EdgeInsets.all(16.00),
              child: Container(
                  child:  buildBody(context,_indexTab)//_motorsListWidget(),
              ));*/
        }).toList());*/
  }

  Widget _floatingButtonWidget(){
    return FloatingActionButton(
        child: Icon(Icons.find_in_page),
        onPressed: () {
          Future<String> futureString = new QRCodeReader()
              .setAutoFocusIntervalInMs(200) // default 5000
              .setForceAutoFocus(true) // default false
              .setTorchEnabled(true) // default false
              .setHandlePermissions(true) // default true
              .setExecuteAfterPermissionGranted(true) // default true
              .scan()
              .then((r) {
            //print({r,'______________________________'});
            if (r != null) {
              /*Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailsView(this
                              ._motorList
                              .singleWhere((motor) =>
                                  motor.code ==
                                  r))), //////////////////////////////Verficar busqueda
                    );*/
            }
          });
        });
  }
}



class Choice {
  final String status;
  final IconData icon;
  const Choice({this.status, this.icon});
}

const List<Choice> choices = const <Choice>[
  const Choice(status: 'NUEVOS', icon: Icons.input),
  const Choice(status: 'REPARACION', icon: Icons.build),
  const Choice(status: 'TERMINADOS', icon: Icons.done)
];
