import 'dart:async';

class MotorBloc{
  final _motorBlocController = StreamController<String>();

//FormFieldValidator
  final statusBarValidator = StreamTransformer<String, String>.fromHandlers(
      handleData: (status,sink){
        if(status != null){
          sink.add(status);
        }
      }
  );


//Add data to a Stream
  Function(String) get changeStatusTab => _motorBlocController.sink.add;

//Retrieve data from Stream
  Stream<String> get statusTab => _motorBlocController.stream;

  void Dispose(){
    _motorBlocController.close();
  }
}
 final motorBloc = MotorBloc();
