import 'dart:async';

class NewMotor{
  //StreamController definition///////////////////////////////////////////////////////
  final _blocNewMotorClientController = StreamController<String>.broadcast();
  final _blocNewMotorDescriptionController = StreamController<String>.broadcast();

  //Form field validator/////////////////////////////////////////////////////////////
  //ClientValidatosr
  final blocNewMotorClientValidator = StreamTransformer<String, String>.fromHandlers(
    handleData: (client, sink){
      if(client.length > 5){
        sink.add(client);
      }else{
        sink.addError("No se ha ingresado un cliente.");
      }
    }
  );
  //DescriptionValidator
  final blocMotorDescriptionValidator = StreamTransformer<String, String>.fromHandlers(
    handleData: (description, sink){
      if(description.length > 5){
        sink.add(description);
      }else{
        sink.addError("No se ha ingresado una descripcion.");
      }
    }
  );

  //I/O/////////////////////////////////////////////////////////////////////////////////
  //Add data to stream
  Function(String) get changeClient => _blocNewMotorClientController.sink.add;
  Function(String) get changeDescription => _blocNewMotorDescriptionController.sink.add;

  Stream<String> get client =>  _blocNewMotorClientController.stream.transform(blocNewMotorClientValidator).asBroadcastStream();
  Stream<String> get description => _blocNewMotorDescriptionController.stream.transform(blocMotorDescriptionValidator).asBroadcastStream();

  //Dispose controllers
  void Dispose(){
    _blocNewMotorClientController.close();
    _blocNewMotorDescriptionController.close();
  }
}

//Global instance
final blocNewMotor = NewMotor();